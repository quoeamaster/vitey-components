import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { resolve } from 'path'
import postcss from 'rollup-plugin-postcss'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],

  build: {
    lib: {
      entry: resolve(__dirname, './src/index.js'),
      formats: ['es', 'cjs'],
      fileName: (format) => `vitey-components.${format}.js`,
    },
    rollupOptions: {
      external: [ 'vue' ],
      output: {
        preserveModules: true, 
        exports: 'named',
      },
      plugins: [
        postcss({
          extract: true,
          extensions: ['css'],
          modules: true,
        }),
      ],
    },
    sourcemap: true,
    emptyOutDir: true,
  },

})
