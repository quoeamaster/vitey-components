# remove the HelloWorld component
rm src/components/HelloWorld.vue

# update style.css
cat << EOF > src/style.css
@tailwind base;
@tailwind components;
@tailwind utilities;

@layer components {
}
EOF

# update the build / export index.js (root level)
cat << EOF > src/index.js
// main entry point for building various components
import * as components from './components'
// include tailwind css and other required css files for building purposes
import './style.css'

const defaultComponents = components?.default
// make the default-components plugin-able into vue's eco-system directly
const viteyComponents = {
  install(Vue) {
    Object.keys(defaultComponents).forEach(name => {
      Vue.component(name, defaultComponents[name])
    })
  }
}
export default viteyComponents

// tree shaking purpose
export { viteycard } from './components/card'
EOF

# create a Card component
mkdir src/components/card

# actual content
cat << EOF > src/components/card/Card.vue
<template>
  <div>
    <div class="text-red-400" @click="onClick('card title')">
      {{title}}
    </div>

    <slot></slot>
  </div>
</template>

<script>
export default {
  emits: ['click'],
  props: {
    title: String,
  },
  methods: {
    onClick: function(id) {
      //console.log(`Card clicked: \${id}`)
      this.\$emit('click', { id, })
    },
  }
}
</script>
EOF

# create the associated card component index.js (exports)
cat << EOF > src/components/card/index.js
import viteyCard from './Card.vue'

export default viteyCard
export { viteyCard }
EOF

# create the component ROOT level index.js (exports)
cat << EOF > src/components/index.js
// ./card = ./card/index.js
import { viteyCard } from './card'

// update the components available to be public accessible (now only has viteyCard)
// this index.js (exports) is purposely prepared for vue's plugin architecture usage. Hint: target project (consuming the components) would need:
// 1. load the module => import viteComponents from 'vitey-components'
// 2. load the css files required => import 'vitey-components/dist/style.css'
// 3. use the components (if you are expecting a global integration with Vue as a plugin) => createApp(App).use(viteyComponents).mount('#app')
export default { viteyCard }
EOF

# update the src/App.vue for local testing of the viteyCard component
cat << EOF > src/App.vue
<script setup>
import { viteyCard } from './components/card'
</script>
<template>
  <div class="mx-20 mt-4">
    <vitey-card title='testing purpose ~ 01' @click="event => console.log(Object.keys(event)+' : happy to get a child component clicked')">
      <div>
        slot contents: could be anyting in HTML syntax
      </div>
    </vitey-card>
  </div>
</template>
EOF

# setup the package.json to be module buildable
# backup if required
mv package.json package.json.ori

cat << EOF > package.json
{
"name": "{your-component-name}",
"private": false,
"version": "1.0.0",
"type": "module",
"main": "./dist/source.cjs.js",
"module": "./dist/source.es.js",
"files": [ "dist" ],
"repository": "{{github-repo-for-the-component}}",
"author": "{{your-name}}",
"license": "AGPL-3.0-or-later",
"scripts": {
  "dev": "vite",
  "build": "vue-tsc && vite build",
  "preview": "vite preview"
},
"devDependencies": {
  "@vitejs/plugin-vue": "^4.5.2",
  "autoprefixer": "^10.4.16",
  "postcss": "^8.4.33",
  "tailwindcss": "^3.4.1",
  "typescript": "^5.2.2",
  "vite": "^5.0.8",
  "vue-tsc": "^1.8.25"
},
"peerDependencies": {
  "vue": "^3.3.11"
}
}
EOF



