// ./card = ./card/index.js
import { viteyCard } from './card'
import { viteyLabel } from './label'

// update the components available to be public accessible (now only has viteyCard)
// this index.js (exports) is purposely prepared for vue's plugin architecture usage. Hint: target project (consuming the components) would need:
// 1. load the module => import viteComponents from 'vitey-components'
// 2. load the css files required => import 'vitey-components/dist/style.css'
// 3. use the components (if you are expecting a global integration with Vue as a plugin) => createApp(App).use(viteyComponents).mount('#app')
export default { viteyCard, viteyLabel, }
