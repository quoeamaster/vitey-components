import { createApp } from 'vue'
import './style.css'
import App from './App.vue'

/* import the fontawesome core */
import { library } from '@fortawesome/fontawesome-svg-core'
/* fa-image resolves as faImage (camel case naming) */
import { faCircleQuestion, faMicrophone, } from '@fortawesome/free-solid-svg-icons';
/* import font awesome icon component */
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

/* 
 * add icons to the library 
 * remember the imported icons earlier must be added to the "library" object for usage
 */
library.add(faCircleQuestion, faMicrophone)

createApp(App).component("fa-icon", FontAwesomeIcon).mount('#app')