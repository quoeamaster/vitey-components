// main entry point for building various components
import * as components from './components'
// include tailwind css and other required css files for building purposes
import './style.css'

const defaultComponents = components?.default
// make the default-components plugin-able into vue's eco-system directly
const viteyComponents = {
  install(Vue) {
    Object.keys(defaultComponents).forEach(name => {
      Vue.component(name, defaultComponents[name])
    })
  }
}
export default viteyComponents

// tree shaking purpose
export { viteyCard } from './components/card'
export { viteyLabel } from './components/label'

// ------------------------ //
// -- font awesome icons -- //
// ------------------------ //

// include for required fontAwesome for build
import { faCircleQuestion, faMicrophone, } from '@fortawesome/free-solid-svg-icons';
const icons = {
  faCircleQuestion,
  faMicrophone,
}
//export { faCircleQuestion, }
export { icons, }


