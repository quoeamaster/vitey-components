#!/bin/bash

# setup tailwindCSS
# install package(s)
yarn add -D tailwindcss postcss autoprefixer

# run npx tailwindCSS init
if command -v npx &> /dev/null
then
   npx tailwindcss init -p
fi

# update the tailwind.config.js
cat << EOF > tailwind.config.js
/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {},
  },
  plugins: [],
}
EOF

# update the src/style.css
echo -e "@tailwind base;\n@tailwind components;\n@tailwind utilities;\n\n$(cat src/style.css)" > src/style.css

# update the src/App.vue to add a div facilitating TailwindCSS
cat << EOF > src/App.vue
<script setup lang="ts">
//import HelloWorld from './components/HelloWorld.vue'
</script>

<template>
  <div class="text-red-400 text-[24px] italic">
    This is a tailwindCSS supported markup.
  </div>
  <div>
    <a href="https://vitejs.dev" target="_blank">
      <img src="/vite.svg" class="logo" alt="Vite logo" />
    </a>
    <a href="https://vuejs.org/" target="_blank">
      <img src="./assets/vue.svg" class="logo vue" alt="Vue logo" />
    </a>
  </div>
  <!--HelloWorld msg="Vite + Vue" /-->
</template>

<style scoped>
.logo {
  height: 6em;
  padding: 1.5em;
  will-change: filter;
  transition: filter 300ms;
}
.logo:hover {
  filter: drop-shadow(0 0 2em #646cffaa);
}
.logo.vue:hover {
  filter: drop-shadow(0 0 2em #42b883aa);
}
</style>
EOF

# install the dependencies
yarn

